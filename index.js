const {findBestMatch} = require('string-similarity');

/**
* Creates an about function
* @param {string} path - A path
* @param {Object} otherData
* @param {string} otherData.preCheck - The message when a command doesn't meet its requirement
* @param {Array<about>} prerequisites - An array of prerequisites that will be checked before determining if it meets this requirement
* @param {function} cb - A method (data) => {} returning a boolean of if the command is allowed to be run
* @return {about} The about function
*/
function about(otherData, prerequisites, cb) {
  return (data, odInfo) => {
    let firstFailure = prerequisites.find(prereq => !prereq(data));

    if(odInfo) {
      if(firstFailure) return firstFailure(data, odInfo);

      let finalData = {};
      for(let key in odInfo) {
        finalData[key] = otherData[key] || "No information available";
        if(typeof finalData[key] === "function") finalData[key] = finalData[key](data)
      }
      return finalData;
    }
    if(firstFailure) return false;
    return cb(data);
  };
}

/** Class representing a command. */
class Command {
  /**
   * Create a command.
   * @constructor
   * @param {object} [options={}] - Options when creating a command
   * @param {string} [options.description=No Description Provided] - The help entry for this command.
   * @param {Array<about>} [options.requirements=[]] - An array of requirements for this command.
   * @param {Array<Array<string>|string>} [options.usage=[]] A usage array. Usage arrays contain strings or arrays of possabilities
   * @param {Object<tbd>} [options.options=[]] TBD. An array of settings for this command.
   * @param {function} [options.callback] A callback when no subcommands are found. Callked with (data) which contains data.parents
   */
  constructor({description = "No description provided", requirements = [], usage = [], options = {}, callback, depricated = false} = {}) {
    this.paths = {};
    this.callback = callback;
    this.description = description;
    this.requirements = requirements;
    this.options = options;
    this.depricated = depricated;
    this.argInfo = usage.map(usagePart => {
      if(Array.isArray(usagePart)) usagePart = usagePart.join`|`;
      return `[${usagePart}]`;
    }).join` ` || ""; // TODO be able to tell subusages about the usages above them
  }
  /**
 * Add a subcommand to this command
 * @param {string} path - A path
 * @param {Command} command - The instance of command to use for this path
 * @return {Command} chainable
 */
  add(path, command) { // TODO add should support parse paths and also things like no path, no path = take their usage and merge it with ours
    this.paths[path] = command;
    return this; //make chainable
  }
  /**
 * Gets the name of this command
 * @param {Array<Command>} parents - An array of all this command's parents
 * @return {string} The name of this command
 */
  myPrefix(parents = []){
    let parentsCopy = parents.slice();
    let myParent = parentsCopy.pop();

    if(!myParent) return false;

    let res = Object.keys(myParent.paths).find(path => {
      let obj = myParent.paths[path]
      return obj === this; // Object compares check if things are the same instance
    });
    return res ? res + " " : ""
  }
  /**
 * Gets the command string a user must type to get this command
 * @param {Array<Command>} parents - An array of all this command's parents
 * @return {string} The string to get this command
 */
  prefix(parents = []) {
    let prefix = "";

    let parentsCopy = parents.slice();
    let myParent = parentsCopy.pop();

    if(myParent) prefix += myParent.prefix(parentsCopy); // I don't know what my prefix is, only my parent does... It's set in add but who knows
    let myPrefix = this.myPrefix(parents);
    if(myPrefix) prefix += myPrefix; // this._prefix should be equal to my name like settings maybe

    return prefix;
  }
  /**
 * Depricates an old command in favor of a new one
 * @param {string} path - The name of the subcommand to depricate
 * @param {string} replacement - The name of the subcommand that replaces it
 * @return {string} The string to get this command
 */
  rename(path, replacement) {
    this.paths[path] = new Command({
      "description": "This command has been renamed",
      "depricated": {replacement: replacement},
      "options": {"showInList": {"user": false, "all": false}}
    }); // TODO YOU CAN"T USE THIS.PREFIX WITHOUT KNOWING PARENTS, preChecks should be allowed to know data too to return something like this.prefix(data.parents)
  }
  /**
 * Determins if and which requirement failed to try and run this command
 * @param {*} data - The data passed to the command when running it
 * @return {?string} The reason the command should not be allowed to run or undefined
 */
  getFailedRequirement(data) {
    let failedRequirement = this.requirements.find(requirement => !requirement(data));
    if(failedRequirement)
      return failedRequirement(data, {"preCheck": ""}).preCheck || "This command could not be run. No reason was specified.";
    return undefined;
  }
  /**
 * Determins if this command should be allowed to be used
 * @param {*} data - The data passed to the command when running it
 * @return {boolean} If this command should be allowed to run
 */
  checkRequirements(data) {
    return !this.getFailedRequirement(data);
  }
  /**
 * Parses a user-inputted command
 * @param {*} data - The data to be passed to the command when running it
 * @param {string} command - The user-inputted command to run
 * @param {Array<Command>} [parents=[]] - The list of parents that are above this command (Used internally/Not required)
 * @return {Promise<Object<defaultMessage, string>>} Returns a an object with some stuff in it sometimes. Who knows, with the quality of these docs anything is possible.
 * @throws Throws an error if the command's callback fails (to prevent unhandledpromiseexceptions)
 */
  async parse(data, command, parents = []) {
    if(this.depricated){ // TODO this.options.depricated? idk something?
      return {type: "renamed", defaultMessage: "This command has been renamed to `"+this.depricated.replacement+"`. Please use that instead", replacement: this.depricated.replacement}
    }

    let failedRequirement = this.getFailedRequirement(data, parents);
    if(failedRequirement) return {type: "preCheckFailed", defaultMessage: failedRequirement, reason: failedRequirement}; // TODO remove defaultMessage or don't or make it non discord based because this should clearly be so generic it doesn't work for my use case

    let cmd = command.split` `;
    let nextPath = cmd.shift();
    if(this.paths[nextPath]) {
      return await this.paths[nextPath].parse(data, cmd.join` `, [...parents, this]);
    }
    if(this.paths[""]){
      cmd = cmd.join` `;
      return await this.paths[""].parse(data, command, [...parents, this]) // todo remove repitition
    }
    // TODO Object.keys(this.paths).sort().find(path => cmd.join` `.startsWith(path)) // TODON'T I TRIED THAT IT WAS HORRIBLE NEVER AGAIN
    if(!this.callback){
      let commandList = this.getUsage({"layers": 2, "data": data, parents: parents});
      let basicFailure = {type: "notFound", defaultMessage: `Command not found. List of commands:\`\`\`${commandList.join`\n`}\`\`\``, commandList: commandList} // TODO allow the user to define this themseves. Easiest method is callbacks but we're doing promises here, so maybe return an object with some information
      if(!nextPath) return basicFailure;

      let mydirectsubcommands = Object.keys(this.paths);
      if(mydirectsubcommands.length === 0) return basicFailure;

      let {bestMatch: {target, rating}} = findBestMatch(nextPath, mydirectsubcommands);

      if(rating < 0.7) return basicFailure;

      return {type: "notFound", defaultMessage: `Command not found. Did you mean: \`${this.prefix(parents) || ""}${target}\``, commandList: commandList, suggestAlternative: `${this.prefix(parents) || ""}${target}`}
    }

    // data.commandCommand = this.getUsage({"layers": 2, "data": data}).join`\n`; // Removed feature
    data.parents = parents;
    let result;
    try{
      result = await this.callback(data, ...command.split` `);
    }catch(er){
      throw er;
    }

    return {type: "success", result: result};
  }
  /**
 * Get the list of subcommands for a command
 * @param {object} [options={}] - Options when getting usage
 * @param {number} [options.layers=-1] - How deep usage should be searched for. Extra-long things will end with ...
 * @param {*} options.data - The data that will passed to the command if run
 * @param {Array<Command>} [parents=[]] - The list of parents that are above this command. Can be gotten from data.parents if called in a command
 * @return {Array<string>} An array of this command's subcommands in a user-callable format
 */
  getUsage({layers = -1, data, parents = []} = {}) {
    if(layers === 0) return [`${this.prefix(parents) || ""}...`];
    if(data && !this.checkRequirements(data, parents)) return [];
    let usage = [];
    if(this.callback) {
      if(this.options.showInList &&
        (data
          ? this.options.showInList.all
          : this.options.showInList.user)
        || !this.options.showInList
      ){
        usage.push(`${this.prefix(parents) || ""}${this.argInfo}`);
      }
    }
    for(let path in this.paths) {
      let belowCommand = this.paths[path].getUsage({"layers": layers - 1, "data": data, parents: [...parents, this]});
      // belowCommand = belowCommand.map(u => `${path} ${u}`);
      usage.push(...belowCommand);
    }
    return usage;
  }
  /**
 * Returns a command at a certain path. For creating commands not calling them
 * @example
 * // Add a list command
 * cmds.add("list", new Command({}))
 * // Add a get subcommand to the list command
 * cmds.path`list`.add("get", new Command({}))
 * @param {string} path - The path to find
 * @return {Command} The command at the path
 * @throws Throws an error if nothing is found at that path
 */
  path(path) {
    path = path.split` `;
    let nextPath = path.shift();
    if(!nextPath) return this;
    nextPath = this.paths[nextPath];
    if(!nextPath) throw new Error("Path not found");
    return nextPath.path(path.join` `);
  }
  static get about() {return about;}
}

module.exports = Command;
