class Router{
  constructor(cb){
    this.cb = cb;
  }
  parse(req, res, next){
    this.cb(req,res,next);
  }
}
