/*global describe it*/

const assert = require("assert");

const Command = require("../index");
const about = Command.about;

let usage = new Command({});
usage.add("settings", new Command({
  "description": "Adjusts settings",
  "requirements": [(o, g) => g ? {"preCheck": "Info needs to be a function to use this command"} : typeof o === "function"]
}));
usage.add("reqtest", new Command({
  "description": "Adjusts settings",
  "requirements": [o => typeof o === "function"]
}));
usage.add("failure", new Command({
  "description": "An async cacllback that fails",
  "requirements": [],
  callback: async (data, ...value) => {
    value = value.join` `;
    if(value) throw new Error(value)
    return "ok"
  }
}));
usage.path("settings").add("rankmoji", new Command({
  "description": "Adjusts Rankmoji",
  "callback": (data) => {
    data("MYrankmoji");
  }
}));
usage.path("settings rankmoji").add("add", new Command({
  "description": "Adds a rankmoji",
  "usage": ["rank", "moji"],
  "callback": (data, rank, ...moji) => {
    data(`${rank  }, ${ moji.join` `.trim()}`);
  }
}));
usage.path("settings rankmoji").add("remove", new Command({
  "description": "Removes a rankmoji",
  "usage": [["rank", "moji"]],
  "requirements": [],
  "callback": (data, ...rankOrMoji) => {
    data(rankOrMoji.join` `.trim());
  }
}));
usage.rename("rankmojiSettings", "settings rankmoji");

describe("Command", () => { // MOCHA DOESN'T WORK WITH ASYNC FUNCTIONS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  it("should give the reason when there is one", async () => {
    assert.deepStrictEqual(await usage.parse("hi", "settings"), {type: "preCheckFailed", defaultMessage: "Info needs to be a function to use this command", "reason": "Info needs to be a function to use this command"});
  });
  it("should give a no reason message when there is no reason", async () => {
    assert.deepStrictEqual(await usage.parse("hi", "reqtest"), {type: "preCheckFailed", defaultMessage: "This command could not be run. No reason was specified.", "reason": "This command could not be run. No reason was specified."});
  });
  it("should show usage when failing", async () => {
    assert.deepStrictEqual(await usage.parse(_=>_, "settings"), {type:"notFound", defaultMessage: `Command not found. List of commands:\`\`\`settings rankmoji${" "}
settings rankmoji add ...
settings rankmoji remove ...\`\`\``, commandList: [
  "settings rankmoji ",
  "settings rankmoji add ...",
  "settings rankmoji remove ..."
]});
  });
  it("should show fuzzy match when failing well", async () => {
    assert.deepStrictEqual(await usage.parse(_=>_, "settings rankMojis"), {type: "notFound", defaultMessage: 'Command not found. Did you mean: `settings rankmoji`', commandList: [
      "settings rankmoji ",
      "settings rankmoji add ...",
      "settings rankmoji remove ..."
    ], suggestAlternative: "settings rankmoji"});
  });
  it("should call the function", async () => {
    assert.deepStrictEqual(await usage.parse(o => {
      let a = 0; // hello coverage for this please
      assert.equal(o, "MYrankmoji")
    }, "settings rankmoji"), {type: "success", result: undefined});
  });
  it("should call the function with arguments", async () => {
    assert.deepStrictEqual(await usage.parse(o => assert.deepStrictEqual(o, "rankID, my moji"), "settings rankmoji add rankID   my moji "), {type: "success", result: undefined});
  });
  it("should", async () => {
    assert.deepStrictEqual(await usage.parse(o => assert.deepStrictEqual(o, "moji to remove"), "settings rankmoji remove   moji to remove   "), {type: "success", result: undefined});
  });
  it("should give messages for renamed commands", async () => {
    assert.deepStrictEqual(await usage.parse("hi", "rankmojiSettings"), {type: "renamed", defaultMessage: "This command has been renamed to `settings rankmoji`. Please use that instead", replacement: "settings rankmoji"});
  });
  it("should throw errors when an async callback fails rather than having an unhandled rejection", async () => {
    assert.deepStrictEqual(await usage.parse({}, "failure"), {type: "success", result: "ok"});
    let a = 0;
    try{
      await usage.parse({}, "failure badly") // Should throw // Can't assert.throws, tried that, no async
    }catch(er){
      a++;
    }
    assert.equal(a, 1);
  });
  it("should error when the path is not found", () => {
    assert.throws(_ => usage.path("settings notfound"));
  });
  it("should support tagged paths", () => {
    assert.throws(_ => usage.path`settings`);
  });
  it("should be able to have superprefixes changed and messed with and still function", async () => {
    let superUsage = new Command({})
    superUsage.add("owow", usage)
    assert.deepStrictEqual(await superUsage.parse("data", "owow hi"), {type: "notFound", defaultMessage: `Command not found. List of commands:\`\`\`owow failure \`\`\``, commandList: ["owow failure "]})
    assert.deepStrictEqual(await usage.parse("data", "hi"), {type: "notFound", defaultMessage: `Command not found. List of commands:\`\`\`failure \`\`\``, commandList: ["failure "]})
  })
  it("should support blank paths and dynamic switching onto these blank paths", async () => {
    let superbOwl = new Command({});
    superbOwl.add("", usage);
    superbOwl.add("test", usage);
    assert.deepStrictEqual(await superbOwl.parse("hmm", ""), {type: "notFound", commandList: ["failure "], defaultMessage: `Command not found. List of commands:\`\`\`failure \`\`\``}) // failure is the only command because wait a second shouldn't there be two idk maybe it works ok bye
    assert.deepStrictEqual(await superbOwl.parse(() => "data", "settings rankmoji add my rank"), {type: "success", result: undefined})
    assert.deepStrictEqual(await superbOwl.parse(() => "data", "test settings rankmoji remove my otherrank"), {type: "success", result: undefined})
  })
// ok
});

describe("about", () => {
  it("should be tested", () => {
    let ab = a => about({"preCheck": "hi", "other": "bye"}, [], b=>a===b); // eslint-disable-line func-style
    assert.equal(ab(true)(true), true);
    assert.equal(ab(false)(false), true);
    assert.equal(ab("yes")(true), false);
    assert.deepStrictEqual(ab(true)(true, {"preCheck": true}), {"preCheck": "hi"});
    assert.deepStrictEqual(ab(true)(true, {"other": true}), {"other": "bye"});
    assert.deepStrictEqual(ab(true)(true, {"preCheck": true, "other": true}), {"preCheck": "hi", "other": "bye"});

    let prereq = (b) => about({"preCheck": "hifail"}, [ab(b)], _=>b); // eslint-disable-line func-style
    assert.ok(prereq("hello")("hello"));
    assert.equal(prereq("hello")("hi"), false);
    assert.deepStrictEqual(prereq("true")("false", {"preCheck": true}), {"preCheck": "hi"});
    assert.deepStrictEqual(prereq(false)(false, {"preCheck": true}), {"preCheck": "hifail"});
    assert.deepStrictEqual(prereq(false)(false, {"test": true}), {"test": "No information available"});
  });
});

/*
settings rankmoji add <rank> <moji>
settings rankmoji remove
 */
